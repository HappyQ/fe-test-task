import React from "react"
import ReactDOM from "react-dom"
import { createBrowserHistory } from "history"
import { Router } from "react-router"
import { Provider as StoreProvider } from "mobx-react"

import stores from "./app/stores"
import App from "./app"
import "./index.scss"

const history = createBrowserHistory()

ReactDOM.render(
  <StoreProvider {...stores}>
    <Router history={history}>
      <App />
    </Router>
  </StoreProvider>,
  document.getElementById("root")
)

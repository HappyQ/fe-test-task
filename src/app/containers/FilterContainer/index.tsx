import React from "react"

import Filter from "../../components/FilterComponent"

interface Props {}

const FilterContainer: React.FC<Props> = () => {
  return <Filter filters={["By Manufacturer", "Minimum price", "Maximum price"]} />
}

export default FilterContainer

import React from "react"
import { inject, observer } from "mobx-react"

import RootStores from "../../stores/model"

import Cart from "../../components/CartComponent"

interface Props {}

const CartContainer: React.FC<Partial<RootStores>> = ({ cartStore }) => {
  return <Cart cartCount={cartStore!.cart.length} />
}

export default inject("cartStore")(observer(CartContainer))

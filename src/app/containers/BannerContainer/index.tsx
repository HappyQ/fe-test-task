import React from "react"

import Banner from "../../components/BannerComponent"

interface Props {}

const BannerContainer: React.FC<Props> = () => {
  return <Banner />
}

export default BannerContainer

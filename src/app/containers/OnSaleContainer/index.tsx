import React from "react"
import { inject, observer } from "mobx-react"

import OnSale from "../../components/OnSaleComponent"
import RootStores from "../../stores/model"

const OnSaleContainer: React.FC<Partial<RootStores>> = ({ cartStore }) => {
  const onProductClicked: (item: any) => void = (item) => {
    cartStore?.addProductToCart(item)
    console.log()
  }
  return <OnSale onProductClicked={onProductClicked} product={cartStore?.cart} />
}

export default inject("cartStore")(observer(OnSaleContainer))

import React from "react"

import Header from "../../components/HeaderComponent"

interface Props {}

const HeaderContainer: React.FC<Props> = () => {
  return <Header />
}

export default HeaderContainer

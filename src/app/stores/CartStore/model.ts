export interface IProduct {
  id: number
  date: string
  manufacturer: string
  model: string
  hash: {
    min: number
    max: number
  }
  algorithm: string
  efficiency: string
  profit: number
  price: {
    min: number
    max: number
  }
}

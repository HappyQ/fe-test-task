import { action, observable } from "mobx"
import { IProduct } from "./model"

export default class CartStore {
  @observable language = "ru"

  @observable cart: Array<IProduct> = []

  @action
  setLanguage = (lang: string): void => {
    this.language = lang
  }

  @action
  addProductToCart = (item: IProduct): void => {
    if (!this.cart.some((product) => product.id === item.id)) {
      this.cart.push(item)
    }
  }
}

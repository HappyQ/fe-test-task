import CartStore from "./CartStore"

export default interface RootStores {
  cartStore: CartStore
}

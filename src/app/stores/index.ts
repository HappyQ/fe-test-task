import RootStores from "./model"
import CartStore from "./CartStore"

const rootStores: RootStores = { cartStore: new CartStore() }

export default rootStores

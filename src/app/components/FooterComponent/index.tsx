import React from "react"

import { ReactComponent as Facebook } from "./assets/icons/facebook.svg"
import { ReactComponent as Twitter } from "./assets/icons/twitter.svg"
import { ReactComponent as Youtube } from "./assets/icons/youtube.svg"
import { ReactComponent as Reddit } from "./assets/icons/reddit.svg"
import Squares from "../SquaresComponent"
import "./style.scss"

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer__icons">
        <div className="footer__icon">
          <Squares />
          <Facebook />
        </div>
        <div className="footer__icon">
          <Squares />
          <Twitter />
        </div>
        <div className="footer__icon">
          <Squares />
          <Youtube />
        </div>
        <div className="footer__icon">
          <Squares />
          <Reddit />
        </div>
      </div>
    </footer>
  )
}

export default Footer

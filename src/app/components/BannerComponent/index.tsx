import React from "react"

import { ReactComponent as Banner } from "./assets/banner.svg"
import Title from "../TitleComponent"
import "./style.scss"

interface Props {}

const BannerComponent: React.FC<Props> = () => {
  return (
    <div className="banner">
      <Title title="news" />
      <Banner className="banner__item" />
    </div>
  )
}

export default BannerComponent

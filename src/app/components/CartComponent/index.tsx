import React from "react"

import { ReactComponent as CartIcon } from "./assets/icons/cart.svg"
import "./style.scss"

interface Props {
  cartCount: number
}

const Cart: React.FC<Props> = ({ cartCount }) => {
  return (
    <div className="cart">
      <CartIcon />
      <div className="cart__count">{cartCount}</div>
    </div>
  )
}

export default Cart

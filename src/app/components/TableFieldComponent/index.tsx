import React from "react"

import { ReactComponent as Star } from "./assets/icons/star.svg"
import { ReactComponent as GreenArrow } from "./assets/icons/green_arrow.svg"
import "./style.scss"

interface Props {
  data: {
    date: string
    manufacturer: string
    model: string
    hash: {
      min: number
      max: number
    }
    algorithm: string
    efficiency: string
    profit: number
    price: {
      min: number
      max: number
    }
  }
}

const TableField: React.FC<Props> = ({ data }) => {
  return (
    <>
      <div className="table-item">
        <div className="table-item__rel">
          <Star />
          <GreenArrow />
          {data.date}
        </div>
      </div>
      <div className="table-item">{data.manufacturer}</div>
      <div className="table-item">{data.model}</div>
      <div className="table-item">{`${data.hash.min}-${data.hash.max}`}</div>
      <div className="table-item">{data.algorithm}</div>
      <div className="table-item">{data.efficiency} j/H/s</div>
      <div className="table-item">${data.profit}/ day</div>
      <div className="table-item">{`$${data.price.min}-$${data.price.max}`}</div>
    </>
  )
}

export default TableField

import React from "react"

import Square from "../SquaresComponent"
import Title from "../TitleComponent"
import "./style.scss"

interface Props {
  filters: Array<string>
}

const FilterComponent: React.FC<Props> = ({ filters }) => {
  return (
    <div className="filter">
      <Title title="Sort by" />
      {filters.map((filter, index) => (
        <div className="filter__field" key={`${index}__${filter}`}>
          <div className="filter__field__description">{filter}</div>
          <Square />
        </div>
      ))}
    </div>
  )
}

export default FilterComponent

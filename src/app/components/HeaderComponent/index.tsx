import React from "react"
import { ReactComponent as Logo } from "./assets/logo/logo.svg"
import Cart from "../../containers/CartContainer"

import "./style.scss"

interface Props {}

const Header = (props: Props) => {
  return (
    <div className="header">
      <Logo />
      <Cart />
    </div>
  )
}

export default Header

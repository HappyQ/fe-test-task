import React from "react"

import Title from "../TitleComponent"
import TableFieldComponent from "../TableFieldComponent"

import "./style.scss"

interface Props {
  onProductClicked: (item: any) => void
  product: any
}

const data = ["Release", "Manufacturer", "Model", "Hash", "Algorithm", "Efficiency", "Profit", "Price"]
const data2 = [
  {
    id: "1",
    date: "Mar 2019",
    manufacturer: "Bitmain",
    hash: {
      min: 10.5,
      max: 14.5,
    },
    algorithm: "SHA-256",
    efficiency: "8.0",
    profit: 122.8,
    price: {
      min: 137,
      max: 217,
    },
    model: "S9i",
  },
  {
    id: "2",
    date: "Mar 2019",
    manufacturer: "Bitmain",
    hash: {
      min: 10.5,
      max: 14.5,
    },
    algorithm: "SHA-256",
    efficiency: "8.0",
    profit: 122.8,
    price: {
      min: 137,
      max: 217,
    },
    model: "S9i",
  },
  {
    id: "3",
    date: "Mar 2019",
    manufacturer: "Bitmain",
    hash: {
      min: 10.5,
      max: 14.5,
    },
    algorithm: "SHA-256",
    efficiency: "8.0",
    profit: 122.8,
    price: {
      min: 137,
      max: 217,
    },
    model: "S9i",
  },
  {
    id: "4",
    date: "Mar 2019",
    manufacturer: "Bitmain",
    hash: {
      min: 10.5,
      max: 14.5,
    },
    algorithm: "SHA-256",
    efficiency: "8.0",
    profit: 122.8,
    price: {
      min: 137,
      max: 217,
    },
    model: "S9i",
  },
]
const OnSaleComponent: React.FC<Props> = ({ onProductClicked, product }) => {
  return (
    <div className="onsale">
      <Title title="On sale" />
      <section className="onsale__table">
        <header className="onsale__table__header">
          {data.map((item, index) => (
            <div className="onsale__table__header-item" key={`${index}__${item}`}>
              {item}
            </div>
          ))}
        </header>
        {data2.map((item, index) => (
          <div
            className="onsale__table__body"
            key={`${index}__${item.price.min + item.price.max + item.model}`}
            onClick={() => {
              onProductClicked(item)
              console.log(product)
            }}
          >
            <TableFieldComponent data={item} />
          </div>
        ))}
      </section>
    </div>
  )
}

export default OnSaleComponent

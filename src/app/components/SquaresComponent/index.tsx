import React from "react"

import "./style.scss"
interface Props {}

const SquaresComponent: React.FC<Props> = () => {
  return (
    <div className="square">
      <div className="square--top-left"></div>
      <div className="square--top-right"></div>
      <div className="square--bottom-left"></div>
      <div className="square--bottom-right"></div>
    </div>
  )
}

export default SquaresComponent

import React from "react"
import { Route, Switch } from "react-router"

import Header from "./containers/HeaderContainer"
import Footer from "./components/FooterComponent"
import Main from "./pages/MainPage"
import "./style.scss"

interface Props {}

const App: React.FC<Props> = () => {
  return (
    <div className="app">
      <Header />
      <Switch>
        <Route path="/" component={Main} />
      </Switch>
      <Footer />
    </div>
  )
}

export default App

import React from "react"

import Filter from "../../containers/FilterContainer"
import OnSale from "../../containers/OnSaleContainer"
import Banner from "../../containers/BannerContainer"
import "./style.scss"

interface Props {}

const Main: React.FC<Props> = () => {
  return (
    <div className="main">
      <Filter />
      <OnSale />
      <Banner />
    </div>
  )
}

export default Main
